//
//  ContentViewController.swift
//  exchange_rates_new
//
//  Created by Владислав on 21.03.16.
//  Copyright © 2016 Vladislav Zhukov. All rights reserved.
//
import UIKit
import Charts
import ERengine

class ContentViewController: UIViewController, ChartViewDelegate {
    
    @IBOutlet weak var valutaLabel: UILabel!
    @IBOutlet weak var valutaCourse: UILabel!
    @IBOutlet weak var trendImage: UIImageView!
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var labelWithUpdateTime: UILabel!
    @IBOutlet weak var labelWithCourseOfValutaForDate: UILabel!
    @IBOutlet weak var labelForChartHeader: UILabel!
    @IBOutlet weak var labelWithValeСhange: UILabel!
    @IBOutlet weak var labelWithNominal: UILabel!
    @IBOutlet weak var labelWithValutaNameRus: UILabel!
    
    var pageIndex : Int!
    var valutaName : String!
    var valutaData : CoursesData!
    
    @IBAction func CalcAction(sender: AnyObject) {
        
        let toShow = self.storyboard?.instantiateViewControllerWithIdentifier("CalcView") as! CalcViewController
        toShow.nominal = valutaData!.todayNominal ?? 0.0
        toShow.value = valutaData!.todayValue ?? 0.0
        toShow.valutaName = valutaName ?? "-"
        presentViewController(toShow, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.labelForChartHeader.text = "Динамика курса \(valutaName) к рублю"
        self.labelWithValutaNameRus.text = ERengine.sharedInstance.valutaRusName[valutaName]
        self.trendImage.image = UIImage(named: "arrowDown")
        self.valutaLabel.text = valutaName
        //self.valutaCourse.text = "--"
        
        setChart()
        
        view.backgroundColor = backgroundColor

        
        ERengine.sharedInstance.getDataForValuta(valutaName: valutaName) { [weak weakSelf = self] (data) in
            
            weakSelf?.valutaData = data
            weakSelf?.updateData()
        }
    }
    
    private func setChart() {
        self.lineChartView.delegate = self
        self.lineChartView.noDataText = "Данные не загружены..."
        self.lineChartView.backgroundColor = backgroundColor
        self.lineChartView.drawBordersEnabled = false
        self.lineChartView.dragDecelerationEnabled = false
        self.lineChartView.drawGridBackgroundEnabled = false
        self.lineChartView.drawMarkers = false
        self.lineChartView.animate(xAxisDuration: 0.3, yAxisDuration: 0.3, easingOption: .Linear)
        
        let leftAxis = self.lineChartView.leftAxis
        let rightAxis = self.lineChartView.rightAxis
        let xAxis = self.lineChartView.xAxis
        
        self.lineChartView.descriptionText = ""

        leftAxis.enabled = false
        rightAxis.enabled = false
        xAxis.enabled = false
        
        self.lineChartView.leftAxis
        
        self.lineChartView.legend.enabled = false
        self.lineChartView.drawGridBackgroundEnabled = false
        self.lineChartView.tintColor = backgroundColor
        
    }
    
    func updateData() {
        

        let valueAsString = String(valutaData!.todayValue) ?? "0.0"
        //self.valutaCourse.text = valueAsString
        let nominalAsString = String(valutaData!.todayNominal) ?? "0"
        self.labelWithNominal.text = nominalAsString + "="
        let dataAsString = valutaData!.dateValue ?? "00/00/0000"
        self.labelWithUpdateTime.text = "Обновлено: \(dataAsString)"
        self.labelWithCourseOfValutaForDate.text = "Курс на " + dataAsString + " " + valueAsString + " руб"
        let unwrappedDifferenceValue = valutaData!.difference ?? 0.0
        let differenceAsString = String(unwrappedDifferenceValue) ?? "0"
        self.labelWithValeСhange.text = differenceAsString
        
        if valutaData!.bValueIsIncreases {
            self.trendImage.image = UIImage(named: "arrowUp")
        } else {
            self.trendImage.image = UIImage(named: "arrowDown")
        }
        
        updateLineChart()
    }
    
    private func updateLineChart() {
        
        var dataEntries : [ChartDataEntry] = []
        var circleColors : [UIColor] = []
        
        for i in 0..<valutaData!.values.count {
            let dataEntry = ChartDataEntry(value: Double(valutaData!.values[i]), xIndex: i)
            dataEntries.append(dataEntry)
            circleColors.append(UIColor.brownColor())
        }
        
        let lineChartDataSet = LineChartDataSet(yVals: dataEntries, label: "")
        lineChartDataSet.circleColors = circleColors
        lineChartDataSet.circleRadius = 5
        lineChartDataSet.circleHoleRadius = 2.5
        lineChartDataSet.fillColor = UIColor.lightGrayColor()
        let lineChartData = LineChartData(xVals: valutaData!.dates, dataSet: lineChartDataSet)
        lineChartView.data = lineChartData
    }
    
    func chartValueSelected(chartView: ChartViewBase, entry: ChartDataEntry, dataSetIndex: Int, highlight: ChartHighlight) {
        
        let valueAsString = String(valutaData!.values[Int(entry.xIndex)]) ?? "0"
        let dataAsString = valutaData!.dates[Int(entry.xIndex)] ?? "00/00/0000"
        self.labelWithCourseOfValutaForDate.text = "Курс на \(dataAsString) \(valueAsString) руб"
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        ERengine.sharedInstance.getDataForValuta(valutaName: valutaName) { [weak weakSelf = self] (data) in
            
            weakSelf?.valutaData = data
            weakSelf?.updateData()
        }
        
    }
    
  }







