//
//  CalcViewController.swift
//  exchangeRates
//
//  Created by Владислав on 17.05.16.
//  Copyright © 2016 MD software. All rights reserved.
//

import UIKit

class CalcViewController: UIViewController {
    
    @IBOutlet weak var value1Value: UILabel!
    @IBOutlet weak var value2Value: UILabel!
    
    @IBOutlet weak var valutaName1: UILabel!
    @IBOutlet weak var valutaName2: UILabel!
    
    var nominal : Float!
    var value : Float!
    var valutaName : String!
    let ruble = "RUB"
    
    
    private var userValue = "0"
    private var calculatedCourse = "0"
    private var direction = true
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        valutaName1.text = valutaName
        valutaName2.text = ruble
        
        view.backgroundColor = backgroundColor
    }
    
    private func updateLables() {
        value1Value!.text! = userValue
        value2Value!.text! = calculatedCourse
    }
    
    private func clacCourse() {
        
        let number = Float(userValue) ?? 0
        let coefficient = value/nominal
        
        var newCalculatedCourse : Float = 0.0
        
        if direction {
            newCalculatedCourse = number * coefficient
        } else {
            newCalculatedCourse = number / coefficient
        }
        
        if newCalculatedCourse == 0 {
            calculatedCourse = "0"
        } else {
            calculatedCourse = String(newCalculatedCourse)
        }
        
        updateLables()
    }
    
    @IBAction func NumberButton(sender: UIButton) {
        
        if(value1Value!.text! == "0")
        {
            userValue = sender.currentTitle!
        } else {
            userValue += sender.currentTitle!
        }
        clacCourse()
    }
    
    @IBAction func cancelAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //FIXME: rename
    @IBAction func refresh(sender: UIButton) {
        
        let temp = valutaName1.text
        
        valutaName1.text = valutaName2.text
        valutaName2.text = temp
        
        if direction {
            direction = false
        } else {
            direction = true
        }
        clacCourse()
    }
    
    @IBAction func OperationAction(sender: UIButton) {
        
        if(sender.currentTitle! == "<-") {
            if(userValue != "0") {
                if userValue.characters.count == 1 {
                    userValue = "0"
                } else {
                    let newUserValue = String(userValue.characters.dropLast()) ?? "0"
                    userValue = newUserValue
                }
            }
        } else if sender.currentTitle! == "." {
            
            if !userValue.containsString(".") {
                userValue += sender.currentTitle!
            }
        }
        clacCourse()
    }
    
}
