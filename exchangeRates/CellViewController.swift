//
//  CellViewController.swift
//  exchangeRates
//
//  Created by Владислав on 05.05.16.
//  Copyright © 2016 MD software. All rights reserved.
//
import UIKit

class CellViewController: UITableViewCell {
    
    @IBOutlet weak var Lable: UILabel!
    @IBOutlet weak var Switch: UISwitch!
    @IBOutlet weak var flagImageOfValuta: UIImageView!
    
    
    @IBAction func SwitchWasSwitched(sender: AnyObject) {
        if Switch.on {
            UserSettings.sharedInstance.possibleListOfValutaAndResources.updateValue(true, forKey: Lable!.text!)
        } else {
            UserSettings.sharedInstance.possibleListOfValutaAndResources.updateValue(false, forKey: Lable!.text!)
        }
    }
}
