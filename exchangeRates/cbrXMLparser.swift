//
//  cbrRateProvider.swift
//  exchange_rates_new
//
//  Created by Владислав on 21.03.16.
//  Copyright © 2016 Vladislav Zhukov. All rights reserved.
//
import UIKit

class ExchangeData
{
    var date : String!
    var nominal : Int!
    var value : String!
    
    init(date: String, nominal : String, value : String) {
        self.date = date
        self.nominal = Int(nominal)
        self.value = value
    }
}

class cbrXMLparser: NSObject, NSXMLParserDelegate
{
    var parser : NSXMLParser!
    
    var resultOfParsing = false
    var todayNominal : Float?
    var todayValue : Float?
    var dateValue : String?
    var values : [Float] = [0.0]
    var dates : [String] = [""]
    
    
    init(data : NSData) {
        
        super.init()
        
        parser = NSXMLParser(data: data)
        parser?.delegate = self
    }
    
    func parse() {
        
        let parsedIsSuccessfully =  parser!.parse()
        if parsedIsSuccessfully {
            convertRawDataToInternalFormat()
            resultOfParsing = true
        }
    }
    
    private func convertRawDataToInternalFormat() {
        
        if let lastObjInData = self.exchangeRawData.last {
            
            todayNominal = Float(lastObjInData.nominal)
            todayValue = Float(lastObjInData.value) ?? 0.0
            dateValue = lastObjInData.date
        }
        
        values.removeAll()
        dates.removeAll()
        
        for obj in self.exchangeRawData {
            
            dates.append(obj.date)
            let numberFloatValue = Float(obj.value) ?? 0.0
            values.append(numberFloatValue)
        }
    }
    
    //MARK: parser
    //vars for parser
    private var exchangeRawData : [ExchangeData] = []
    private var value : String = ""
    private var nominal : String = ""
    private var date : String = ""
    private var passName : Bool = false
    private var currentElement : String = ""
    private var currentKey : String = ""
    
    @objc internal func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        
        passName = false
        currentElement = elementName;
        
        if(elementName == "Nominal" || elementName == "Value")
        {
            
            passName = true
            currentKey = elementName
            
        } else if (elementName == "Record") {
            
            if let date : String = attributeDict["Date"] {
                self.date = date
            }
        }
    }
    
    @objc internal func parser(parser: NSXMLParser, foundCharacters string: String) {
        
        if(passName) {
            
            if(currentKey == "Nominal") {
                nominal = string
            }
            
            if(currentKey == "Value") {
                value = string.stringByReplacingOccurrencesOfString(",", withString: ".")
            }
        }
    }
    
    @objc internal func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if(elementName == "Record")
        {
            let object = ExchangeData(date:date, nominal: nominal, value: value)
            exchangeRawData.append(object)
        }
    }
    
    @objc internal func parser(parser: NSXMLParser, parseErrorOccurred parseError: NSError) {
        NSLog("failure error: %@", parseError)
    }
}
