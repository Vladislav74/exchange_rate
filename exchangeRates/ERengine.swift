//
//  ERengine.swift
//  exchangeRates
//
//  Created by Владислав on 02.06.16.
//  Copyright © 2016 MD software. All rights reserved.
//

import Foundation


public class ERengine {
    
    public class var sharedInstance : ERengine {
        struct Singleton {
            static let instance = ERengine()
        }
        return Singleton.instance
    }
    
    //MARK: constants and strings
    private let urlForDynamicDataFromCbrRu = "http://www.cbr.ru/scripts/XML_dynamic.asp"
    //example for Dynamic Data From Cbr Ru : http://www.cbr.ru/scripts/XML_dynamic.asp?date_req1=02/03/2001&date_req2=14/03/2001&VAL_NM_RQ=R01235
    
    private let daysInterval = 14
    
    private let valutaCodesForNames =  ["USD" : "R01235",
                                        "EUR" : "R01239",
                                        "KZT" : "R01335",
                                        "UAH" : "R01720",
                                        "CHF" : "R01775",
                                        "BYR" : "R01090",
                                        "GBP" : "R01035"]
    
    public let valutaRusName = ["USD" : "Доллар США",
                         "EUR" : "Евро",
                         "KZT" : "Тенге",
                         "UAH" : "Гривна",
                         "CHF" : "Швейцарский Франк",
                         "BYR" : "Беларуский рубль",
                         "GBP" : "Британский Фунт"]
    
    public let valutaFlagImageName =  ["USD" : "USD",
                                "EUR" : "EUR",
                                "KZT" : "KZT",
                                "UAH" : "UAH",
                                "CHF" : "CHF",
                                "BYR" : "BYR",
                                "GBP" : "GBP"]
    
    
    private var dataStore = CoursesDataStore()
    

    
    private func getDataForValutaAndPutInDataStore(valutaName vn : String) {
        
        obtainDataForValuta(valutaName: vn, operation: { [weak weakSelf = self] data in
            weakSelf?.dataStore.addNewValutaCourseData(valutaName: vn, data: data)
            })
    }
    
    public func getDataForValuta(valutaName vn : String, operation: (CoursesData) -> ()) {
        
        let data = dataStore.coursesData[vn]
        
        if data != nil {
            
            let today = NSDate()
            
            let userCalendar = NSCalendar.currentCalendar()
            
            if userCalendar.compareDate(data!.dayOfUpdate, toDate: today, toUnitGranularity: .Day) == .OrderedSame {
                operation(data!)
                
            } else {
                obtainDataForValuta(valutaName: vn, operation: { [weak weakSelf = self] data in
                    weakSelf?.dataStore.updateDataForValuta(valutaName: vn, data: data)
                    operation(data)
                    })
            }
        } else {
            obtainDataForValuta(valutaName: vn, operation: { [weak weakSelf = self] data in
                weakSelf?.dataStore.addNewValutaCourseData(valutaName: vn, data: data)
                operation(data)
                })
        }
    }
    
    private func obtainDataForValuta(valutaName vn : String, operation: (CoursesData) -> ()) {
        
        let requestUrl = NSURL(string: prepareRequestFor(valutaName: vn))
        if requestUrl != nil {
            dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)) {
                
                let request = NSURLRequest(URL: requestUrl!)
                let session = NSURLSession.sharedSession()
                let task = session.dataTaskWithRequest(request) {
                    (data, response, error) -> Void in
                    if error == nil {
                        if let newData = data {
                            
                            let parser = cbrXMLparser(data : newData)
                            parser.parse()
                            
                            if parser.resultOfParsing {
                                
                                let todayNominal = parser.todayNominal ?? 0
                                let todayValue = parser.todayValue ?? 0
                                let dateValue = parser.dateValue ?? "--"
                                let values = parser.values ?? [0]
                                let dates = parser.dates ?? ["--"]
                                let today = NSDate()
                                var bValueIsIncreases = false
                                var difference : Float = 0.0
                                
                                let index = values.count - 1
                                if index != 0 {
                                    
                                    let value = values[index]
                                    let oldValue = values[(index - 1)]
                                    if value > oldValue {
                                        bValueIsIncreases = true
                                    }
                                    difference = todayValue - oldValue
                                }
                                
                                let data = CoursesData(todayNominal: todayNominal,
                                                       todayValue: todayValue,
                                                       difference: difference,
                                                       dateValue: dateValue,
                                                       values: values,
                                                       dates: dates,
                                                       bValueIsIncreases: bValueIsIncreases,
                                                       dayOfUpdate: today)
                                
                                dispatch_async(dispatch_get_main_queue()) {
                                    operation(data)
                                }
                            }
                        }
                    }
                }
                task.resume()
            }
        }
    }
    
    private func prepareRequestFor(valutaName vN : String) -> String {
        var urlString = urlForDynamicDataFromCbrRu
        
        let (startDateString, endDateString) = getDatesForRequest()
        let valutaCode = valutaCodesForNames[vN] ?? "R01235" //if some error, we just set USD code
        
        urlString += "?date_req1="
        urlString += startDateString
        urlString += "&date_req2="
        urlString += endDateString
        urlString += "&VAL_NM_RQ="
        urlString += valutaCode
        
        return urlString
    }
    
    private func getDatesForRequest() -> (String, String) {
        
        let today = NSDate()
        let dateComponents = NSDateComponents()
        let сalendar = NSCalendar.currentCalendar()
        let CalendarOption = NSCalendarOptions()
        
        dateComponents.day = -daysInterval
        
        let newDay = сalendar.dateByAddingComponents(dateComponents, toDate: today, options: CalendarOption)
        
        let formatter = NSDateFormatter()
        formatter.dateFormat  = "dd/MM/yyyy";
        
        var previousDayStr = formatter.stringFromDate(today)
        
        if let previousDay = newDay {
            previousDayStr = formatter.stringFromDate(previousDay)
        }
        
        let todayStr = formatter.stringFromDate(today)
        
        return (previousDayStr, todayStr)
    }
    
}
