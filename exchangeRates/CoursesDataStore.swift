//
//  CoursesDataStore.swift
//  exchangeRates
//
//  Created by Владислав on 05.06.16.
//  Copyright © 2016 MD software. All rights reserved.
//

import Foundation


public struct CoursesData {
    public var todayNominal : Float
    public var todayValue : Float
    public var difference : Float
    public var dateValue : String
    public var values : [Float]
    public var dates : [String]
    public var bValueIsIncreases : Bool
    public var dayOfUpdate : NSDate
}

class CoursesDataStore {
    
    var coursesData = [String : CoursesData]()
    
    func addNewValutaCourseData(valutaName vn : String, data : CoursesData) {
        
        coursesData.updateValue(data, forKey: vn)
    }
    
    func updateDataForValuta(valutaName vn : String, data : CoursesData) {
        
        coursesData.updateValue(data, forKey: vn)
    }
}





